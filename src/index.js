import React from 'react'
import { render } from 'react-dom'

import './index.css'

import { App } from './Component/App'

const rootElement = document.getElementById('root')
if (!rootElement) {
  throw Error('You must create a #root element')
}

render(<App name="John Doe" />, rootElement)

import { configureStore } from '@reduxjs/toolkit'
import createSagaMiddleware from 'redux-saga'
import * as Effect from '../Effect/index'
import * as SurveyList from './SurveyList'
import * as Survey from './Survey'
import * as Auth from './Auth'

export const createStore = () => {
  const sagaMiddleware = createSagaMiddleware()
  const store = configureStore({
    reducer: {
      surveyList: SurveyList.reducer,
      survey: Survey.reducer,
      auth: Auth.reducer,
    },
    devTools: true,
    middleware: [sagaMiddleware],
  })

  sagaMiddleware.run(Effect.rootSaga)

  return store
}

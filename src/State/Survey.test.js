import * as Survey from './Survey'

test(`
  it reduces to initial state when no action
  and state are specified
`, () => {
  const state = Survey.reducer(undefined, {})

  expect(state).toEqual(Survey.INITIAL_STATE)
})

test('it reduces the change form data action', () => {
  const changeUsername = Survey.changeFormData({
    name: 'username',
    value: 'foo',
  })
  const changeAnswer = Survey.changeFormData({
    name: 'answer',
    value: 1,
  })
  const errorUsername = Survey.changeFormData({
    name: 'username',
    errors: ['Too short username'],
  })

  const actions = [changeUsername, changeAnswer, errorUsername]

  const state = actions.reduce(Survey.reducer, Survey.INITIAL_STATE)

  expect(state).toEqual({
    ...Survey.INITIAL_STATE,
    formData: {
      ...Survey.INITIAL_STATE.formData,
      username: {
        value: 'foo',
        errors: ['Too short username'],
      },
      answer: {
        value: 1,
        errors: [],
      },
    },
  })
})

test('it reduces the fetch survey action', () => {
  const action = Survey.fetch()
  const state = Survey.reducer(undefined, action)

  expect(state).toEqual({ ...Survey.INITIAL_STATE, isLoading: true })
})

test('it reduces the receive survey action', () => {
  const survey = {
    id: 3,
    title: 'test',
    answers: ['Yes', 'Maybe', 'Np'],
  }

  const action = Survey.receive(survey)
  const state = Survey.reducer(undefined, action)

  expect(state).toEqual({
    ...Survey.INITIAL_STATE,
    isLoading: false,
    title: survey.title,
    answers: survey.answers,
  })
})

test('it reduces the submit and fail action', () => {
  const firstError = Survey.fail(Error('Nooooo'))
  const secondError = Survey.fail(Error(':('))

  const firstState = [firstError, secondError].reduce(
    Survey.reducer,
    Survey.INITIAL_STATE
  )

  const submit = Survey.submit()
  const secondState = Survey.reducer(firstState, submit)

  expect(firstState).toEqual({
    ...Survey.INITIAL_STATE,
    errors: ['Nooooo', ':('],
    isLoading: false,
  })

  expect(secondState).toEqual({
    ...Survey.INITIAL_STATE,
    errors: [],
    isLoading: false,
    isSending: true,
  })
})

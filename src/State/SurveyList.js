import { createSlice } from '@reduxjs/toolkit'

export const INITIAL_STATE = {
  isLoading: true,
  error: null,
  data: [],
}

const slice = createSlice({
  name: 'surveyList',
  initialState: INITIAL_STATE,
  reducers: {
    fetch: state => ({
      ...state,
      error: null,
      isLoading: true,
    }),
    fail: (state, { payload: error }) => ({
      ...state,
      error: error.message,
      isLoading: false,
    }),
    receive: (state, { payload: data }) => ({
      ...state,
      data,
      error: null,
      isLoading: false,
    }),
  },
})

export const { reducer } = slice
export const { fetch, fail, receive } = slice.actions

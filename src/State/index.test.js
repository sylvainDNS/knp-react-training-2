import * as State from './index'
import * as Survey from './Survey'
import * as SurveyList from './SurveyList'
import * as Auth from './Auth'

test('it contains the survey list state / reducer', () => {
  const store = State.createStore()
  const state = store.getState()

  expect(state.survey).toEqual(Survey.INITIAL_STATE)
  expect(state.surveyList).toEqual(SurveyList.INITIAL_STATE)
  expect(state.auth).toEqual(Auth.INITIAL_STATE)
})

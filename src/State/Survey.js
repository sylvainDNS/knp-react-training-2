import { createSlice } from '@reduxjs/toolkit'
import { createFormDataField, createFormDataSliceReducer } from '../Util/State'

export const INITIAL_STATE = {
  isLoading: true,
  isSending: false,
  title: null,
  answers: null,
  errors: [],
  formData: {
    ...createFormDataField('answer'),
    ...createFormDataField('username'),
  },
}

const slice = createSlice({
  name: 'survey',
  initialState: INITIAL_STATE,
  reducers: {
    ...createFormDataSliceReducer(),

    fetch: state => ({
      ...state,
      isLoading: true,
      isSending: false,
    }),
    receive: (state, { payload: { title, answers } }) => ({
      ...state,
      title: title,
      answers: answers,
      isLoading: false,
    }),
    fail: (state, { payload: error }) => ({
      ...state,
      errors: [...state.errors, error.message],
      isLoading: false,
    }),
    submit: state => ({
      ...state,
      isSending: true,
      errors: [],
    }),
  },
})

export const { reducer } = slice
export const { fetch, fail, receive, changeFormData, submit } = slice.actions

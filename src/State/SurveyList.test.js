import * as SurveyList from './SurveyList'

test(`
  it reduces to initial state
  when no state has been specified
`, () => {
  const state = SurveyList.reducer(undefined, {})
  expect(state).toEqual(SurveyList.INITIAL_STATE)
})

test(`
  it reduces fetch survey list action
`, () => {
  const action = SurveyList.fetch()
  const state = SurveyList.reducer(SurveyList.INITIAL_STATE, action)

  expect(state).toEqual({
    ...SurveyList.INITIAL_STATE,
    error: null,
    isLoading: true,
  })
})

test(`
  it reduces received survey list action
`, () => {
  const data = ['data']

  const action = SurveyList.receive(data)
  const state = SurveyList.reducer(SurveyList.INITIAL_STATE, action)

  expect(state).toEqual({
    ...SurveyList.INITIAL_STATE,
    data,
    error: null,
    isLoading: false,
  })
})

test(`
  it reduces fail action
`, () => {
  const message = 'Error :('

  const action = SurveyList.fail(Error(message))
  const state = SurveyList.reducer(SurveyList.INITIAL_STATE, action)

  expect(state).toEqual({
    ...SurveyList.INITIAL_STATE,
    error: message,
    isLoading: false,
  })
})

import React from 'react'
import { Link, useParams } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'
import { useActions } from '../Util/Hook'

import { OnlyFor } from './Security'
import * as SurveyState from '../State/Survey'

import { Loader } from './Partial/Loader'
import { ErrorMessage } from './Partial/ErrorMessage'

export const Survey = () => {
  const { id } = useParams()
  const title = useSelector(state => state.survey.title)
  const answers = useSelector(state => state.survey.answers)
  const errors = useSelector(state => state.survey.errors)
  const isLoading = useSelector(state => state.survey.isLoading)
  const isSending = useSelector(state => state.survey.isSending)
  const dispatch = useDispatch()

  useActions([SurveyState.fetch(id)], [id, dispatch])

  return (
    <>
      {isLoading && <Loader />}
      {errors.length > 0 &&
        errors.map((error, index) => (
          <ErrorMessage
            key={`error-message-survey-${index}`}
            onClick={() => dispatch(SurveyState.fetch(id))}
          >
            {error}
          </ErrorMessage>
        ))}
      {!isLoading && !errors.length && (
        <>
          <h1>{title}</h1>
          <form
            onSubmit={e => {
              e.preventDefault()
              dispatch(SurveyState.submit())
            }}
          >
            <OnlyFor
              role="ADMIN"
              FallbackComponent={() => <p>Choose your answer:</p>}
            >
              <p>Choose your answers master of the world:</p>
            </OnlyFor>
            {answers.map((answer, index) => (
              <div key={`survey-answer-${index}`}>
                <input
                  id={`answer-${answer}`}
                  type="radio"
                  name="answer"
                  onClick={() =>
                    dispatch(
                      SurveyState.changeFormData({
                        name: 'answer',
                        value: index,
                      })
                    )
                  }
                />
                <label htmlFor={`answer-${answer}`}>{answer}</label>
              </div>
            ))}
            <div>
              <input
                type="text"
                placeholder="Please, specify your name"
                onChange={e =>
                  dispatch(
                    SurveyState.changeFormData({
                      name: 'username',
                      value: e.target.value,
                    })
                  )
                }
              />
            </div>
            <div>
              <button type="submit">Send</button>
            </div>
          </form>
          {isSending && <div>Sending form !</div>}
        </>
      )}
      <Link to="/">Back</Link>
    </>
  )
}

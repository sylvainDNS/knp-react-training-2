import React from 'react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import { Provider } from 'react-redux'

import * as State from '../State'

import { AuthProvider } from './Security'
import { Survey } from './Survey'
import { SurveyList } from './SurveyList'

import './App.css'

export const App = () => (
  <Router>
    <Provider store={State.createStore()}>
      <AuthProvider>
        <div className="App">
          <header></header>
          <div>
            <div className="content">
              <Switch>
                <Route path="/" component={SurveyList} exact />
                <Route path="/survey/:id" component={Survey} exact />
              </Switch>
            </div>
          </div>
          <footer></footer>
        </div>
      </AuthProvider>
    </Provider>
  </Router>
)

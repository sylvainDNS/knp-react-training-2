import React from 'react'
import { render } from '@testing-library/react'
import { ErrorMessage } from './ErrorMessage'

test('Display an error message and a retry button', () => {
  let clicked = false
  const error = Error('oups')

  const { getByText } = render(
    <ErrorMessage
      onClick={() => {
        clicked = true
      }}
    >
      {error.message}
    </ErrorMessage>
  )

  expect(getByText('oups').tagName).toEqual('H4')
  expect(getByText('Retry').tagName).toEqual('BUTTON')
})

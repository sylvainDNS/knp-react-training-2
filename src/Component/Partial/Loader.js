import React from 'react'

export const Loader = () => (
  <div>
    <h2>Loading...</h2>
  </div>
)

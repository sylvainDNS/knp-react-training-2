import React from 'react'
import { Link } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'
import { useActions } from '../Util/Hook'

import * as SurveyListState from '../State/SurveyList'

import { ErrorMessage } from './Partial/ErrorMessage'
import { Loader } from './Partial/Loader'

import './SurveyList.css'

export const SurveyList = () => {
  const surveyList = useSelector(state => state.surveyList.data)
  const isLoading = useSelector(state => state.surveyList.isLoading)
  const error = useSelector(state => state.surveyList.error)
  const dispatch = useDispatch()

  useActions([SurveyListState.fetch()])

  return (
    <>
      {isLoading && <Loader />}

      {!isLoading && error && (
        <ErrorMessage onClick={() => dispatch(SurveyListState.fetch())}>
          {error}
        </ErrorMessage>
      )}

      {!isLoading && !error && (
        <div className="SurveyList">
          {surveyList.map(({ id, author, createdAt, title }) => (
            <Link key={`survey-thumbnail-${id}`} to={`/survey/${id}`}>
              <div className="card">
                <p className="header">{title}</p>
                <div className="footer">
                  <p>
                    By {author.firstname} {author.lastname}
                  </p>
                  <p>Created at {new Date(createdAt).toDateString()}</p>
                </div>
              </div>
            </Link>
          ))}
        </div>
      )}
    </>
  )
}

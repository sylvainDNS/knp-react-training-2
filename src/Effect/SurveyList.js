import * as Eff from 'redux-saga/effects'
import * as Api from '../Util/Api'
import * as SurveyList from '../State/SurveyList'

export function* fetchSurveySaga() {
  try {
    const surveys = yield Eff.call(Api.fetchSurveys)

    yield Eff.put(SurveyList.receive(surveys))
  } catch (error) {
    yield Eff.put(SurveyList.fail(error))
  }
}

export function* rootSaga() {
  yield Eff.takeEvery(SurveyList.fetch, fetchSurveySaga)
}

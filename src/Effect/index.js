import * as Eff from 'redux-saga/effects'
import * as SurveyList from './SurveyList'
import * as Survey from './Survey'

export function* rootSaga() {
  yield Eff.all([Eff.fork(SurveyList.rootSaga), Eff.fork(Survey.rootSaga)])
}
